package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {

    Booking bookingR;
    Booking bookingS;
    Booking bookingA;
    Booking bookingC;

    @BeforeEach
    void setUp() {
        bookingR = new Booking("r111-08:30am-11:00am");
        bookingS = new Booking("s111-08:30am-11:00am");
        bookingA = new Booking("a111-08:30am-11:00am");
        bookingC = new Booking("c111-08:30am-11:00am");
    }

    @Test
    void parseShouldTakeInBookingCodeAndParseToEachVariable() {
        assertEquals("111", bookingR.getRoomNumber());
        assertEquals("08:30am", bookingR.getStartTime());
        assertEquals("11:00am", bookingR.getEndTime());
    }

    @Test
    void getRoomNumberShouldReturnRoomNumber() {
        assertEquals("111", bookingR.getRoomNumber());
    }

    @Test
    void getStartTimeShouldReturnStartTime() {
        assertEquals("08:30am", bookingR.getStartTime());
    }

    @Test
    void getEndTimeShouldReturnEndTime() {
        assertEquals("11:00am", bookingR.getEndTime());
    }
}