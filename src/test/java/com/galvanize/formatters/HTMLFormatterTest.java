package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HTMLFormatterTest {

    Booking booking;

    @BeforeEach
    void setUp() {
        booking = new Booking("r111-08:30am-11:00am");
    }

    @Test
    void format() {
        HTMLFormatter htmlFormatter = new HTMLFormatter();
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>111</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>";
        assertEquals(expected, htmlFormatter.format(booking));
    }



}