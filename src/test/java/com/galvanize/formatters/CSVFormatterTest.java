package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVFormatterTest {

    Booking booking;

    @BeforeEach
    void setUp() {
        booking = new Booking("r111-08:30am-11:00am");
    }

    @Test
    void format() {
        CSVFormatter csvFormatter = new CSVFormatter();
        String expected = "type,room number,start time,end time\n" +
                "Conference Room,111,08:30am,11:00am";
        String actual = csvFormatter.format(booking);
        assertEquals(expected, actual);
    }


}