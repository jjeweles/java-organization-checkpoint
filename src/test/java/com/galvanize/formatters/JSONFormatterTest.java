package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JSONFormatterTest {

    Booking booking;

    @BeforeEach
    void setUp() {
        booking = new Booking("r111-08:30am-11:00am");
    }

    @Test
    void format() {
        JSONFormatter jsonFormatter = new JSONFormatter();
        String expected = "{\n" +
                "  \"type\": \"Conference Room\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}";
        assertEquals(expected, jsonFormatter.format(booking));
    }

}