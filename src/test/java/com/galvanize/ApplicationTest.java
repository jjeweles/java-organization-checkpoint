package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    void getFormatterShouldReturnJSONFormatter() {
        Application.getFormatter("json");
        assertEquals(JSONFormatter.class, Application.getFormatter("json").getClass());
        Application.getFormatter("csv");
        assertEquals(CSVFormatter.class, Application.getFormatter("csv").getClass());
        Application.getFormatter("html");
        assertEquals(HTMLFormatter.class, Application.getFormatter("html").getClass());
    }

}