package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter {

    @Override
    public String format(Booking booking) {

        return "{\n" +
                "  \"type\": \"" + booking.getRoomType()  + "\",\n" +
                "  \"roomNumber\": " + booking.getRoomNumber() + ",\n" +
                "  \"startTime\": \"" + booking.getStartTime() + "\",\n" +
                "  \"endTime\": \"" + booking.getEndTime() + "\"\n" +
                "}";
    }
}