package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter {

    @Override
    public String format(Booking booking) {

        return "type,room number,start time,end time\n" +
                booking.getRoomType() + "," + booking.getRoomNumber() + "," +
                booking.getStartTime() + "," + booking.getEndTime();
    }
}
