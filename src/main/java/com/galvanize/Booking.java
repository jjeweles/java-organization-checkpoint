package com.galvanize;

public class Booking {

    private enum RoomType {
//        CONFERENCE_ROOM("Conference Room"),
//        SUITE("Suite"),
//        AUDITORIUM("Auditorium"),
//        CLASSROOM("Classroom");

        CONFERENCE_ROOM{
            @Override
            public String toString() { return "Conference Room"; }},
        SUITE{
            @Override
            public String toString() {
                return "Suite";
            }},
        AUDITORIUM{
            @Override
            public String toString() {
                return "Auditorium";
            }},
        CLASSROOM{
            @Override
            public String toString() {
                return "Classroom";
            }}
    }

    private final RoomType roomType;
    private final String roomNumber;
    private final String startTime;
    private final String endTime;

    public Booking(String bookingCode) {
        Booking booking = Booking.parse(bookingCode);
        this.roomType = booking.getRoomType();
        this.roomNumber = booking.getRoomNumber();
        this.startTime = booking.getStartTime();
        this.endTime = booking.getEndTime();
    }

    public Booking(RoomType roomType, String roomNumber, String startTime, String endTime) {
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    static Booking parse(String bookingCode) {
        RoomType roomTypeParse = null;
        String roomNumberParse;
        String startTimeParse;
        String endTimeParse;

        String[] bookingCodeArray = bookingCode.split("-");
        // get room type from bookingCodeArray
        String roomTypeChar = bookingCodeArray[0];
        roomTypeChar = roomTypeChar.substring(0, 1);
        
        switch (roomTypeChar) {
            case "r":
                roomTypeParse = RoomType.CONFERENCE_ROOM;
                break;
            case "s":
                roomTypeParse = RoomType.SUITE;
                break;
            case "a":
                roomTypeParse = RoomType.AUDITORIUM;
                break;
            case "c":
                roomTypeParse = RoomType.CLASSROOM;
                break;
            default:
                break;
        }

        // get room number from bookingCodeArray
        roomNumberParse = bookingCodeArray[0].substring(1);

        // get start time from bookingCodeArray
        startTimeParse = bookingCodeArray[1];

        // get end time from bookingCodeArray
        endTimeParse = bookingCodeArray[2];

        return new Booking(roomTypeParse, roomNumberParse, startTimeParse, endTimeParse);
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
