package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

public class Application {
    public static void main(String[] args) {

        // r111-08:30am-11:00am html
        // s111-08:30am-11:00am json
        // a111-08:30am-11:00am csv
        System.out.println(getFormatter(args[1]).format(new Booking(args[0])));

        //Formatter html = getFormatter("html");
        //System.out.println(html);
        //System.out.printf(html.format(new Booking("r44-8am-11am")));

    }

    static Formatter getFormatter(String format) {
        switch (format) {
            case "json":
                // System.out.println("JSON Formatter");
                return new JSONFormatter();
            case "csv":
                // System.out.println("CSV Formatter");
                return new CSVFormatter();
            case "html":
                // System.out.println("HTML Formatter");
                return new HTMLFormatter();
            default:
                // System.out.println("No working " + format + " formatter available");
                return null;
        }
    }
}